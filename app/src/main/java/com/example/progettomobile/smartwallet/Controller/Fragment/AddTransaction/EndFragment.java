package com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Controller.MainActivity;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Type;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class EndFragment extends Fragment {

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    View mRootView;
    LinearLayout mLayoutEndButtons;
    TextView mTextViewNameTransaction, mTextViewShopName, mTextViewShopAddress, mTextViewDateTransaction, mTextViewCategory, mTextViewTotalAmount;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_end, container, false);

        mLayoutEndButtons = mRootView.findViewById(R.id.layoutEndButtons);
        mTextViewNameTransaction = mRootView.findViewById(R.id.textViewNameTransaction);
        mTextViewShopName = mRootView.findViewById(R.id.textViewShopName);
        mTextViewShopAddress = mRootView.findViewById(R.id.textViewShopAddress);
        mTextViewDateTransaction = mRootView.findViewById(R.id.textViewDateTransaction);
        mTextViewCategory = mRootView.findViewById(R.id.textViewCategory);
        mTextViewTotalAmount = mRootView.findViewById(R.id.textViewTotalAmount);

        mRootView.findViewById(R.id.buttonEndAddTransaction).setOnClickListener(v -> {
            TransactionManager.clearManager();
            startActivity(new Intent(getActivity(), MainActivity.class));
        });

        mRootView.findViewById(R.id.buttonSaveAddTransaction).setOnClickListener(v -> {
            if (TransactionManager.checkImportantFields()) {
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mAuth = FirebaseAuth.getInstance();

                if (Utility.isNetworkAvailable(getContext())) {
                    if (!ListsManager.isDoing()) {
                        try {
                            Date data = new SimpleDateFormat("dd MMM yyyy").parse(TransactionManager.getDate());
                            mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child(Utility.getMonthYear(data)).child(TransactionManager.getID()).setValue(true);
                            double x = Type.valueOf(TransactionManager.getType()) == Type.INCOME ? TransactionManager.getAmountOfTheMonth() + TransactionManager.getAmount() : TransactionManager.getAmountOfTheMonth() - + TransactionManager.getAmount();
                            mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child(Utility.getMonthYear(data)).child("amount").setValue(x);
                        } catch (ParseException e) {
                        }

                        mDatabase.child("transactions").child(TransactionManager.getID()).setValue(TransactionManager.getCurrentTransaction())
                                .addOnSuccessListener(aVoid -> {
                                    showToast(getResources().getString(R.string.success_add_transaction_firebase));
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    Objects.requireNonNull(getActivity()).finish();
                                })
                                .addOnFailureListener(e -> showToast(getResources().getString(R.string.failure_add_transaction_firebase)));
                    } else {
                        mDatabase.child("lists").child(ListsManager.getID()).child("transactions").child(TransactionManager.getID()).setValue(true);
                        mDatabase.child("transactions").child(TransactionManager.getID()).setValue(TransactionManager.getCurrentTransaction())
                                .addOnSuccessListener(aVoid -> {
                                    showToast(getResources().getString(R.string.success_add_transaction_firebase));
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    Objects.requireNonNull(getActivity()).finish();
                                })
                                .addOnFailureListener(e -> showToast(getResources().getString(R.string.failure_add_transaction_firebase)));

                        ListsManager.setStateNone();
                    }
                } else {
                    showToast("Non sei connesso alla rete.");
                }
            } else {
                showToast(getResources().getString(R.string.error_with_current_transaction));
            }
        });

        showFinalReceipt();

        return mRootView;
    }

    public void showFinalReceipt() {
        mLayoutEndButtons.setVisibility(View.VISIBLE);

        mTextViewNameTransaction.setText(TransactionManager.getNameTransaction());
        mTextViewShopName.setText(getResources().getString(R.string.shop_name_end) + TransactionManager.getNameShop());
        mTextViewShopAddress.setText(getResources().getString(R.string.shop_address_end) + TransactionManager.getAddressShop());
        mTextViewDateTransaction.setText(getResources().getString(R.string.data_transaction_end) + TransactionManager.getDate());
        mTextViewCategory.setText(getResources().getString(R.string.category_transaction_end) + TransactionManager.getCategory());
        mTextViewTotalAmount.setText(getResources().getString(R.string.amount_end) + Double.toString(TransactionManager.getAmount()));
    }

    private void showToast(final String message) {
        Snackbar.make(mRootView.findViewById(R.id.coordinatorEndFragment),
                message,
                Snackbar.LENGTH_LONG).show();
    }
}
