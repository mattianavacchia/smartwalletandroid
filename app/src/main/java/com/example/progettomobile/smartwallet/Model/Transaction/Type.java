package com.example.progettomobile.smartwallet.Model.Transaction;

public enum Type {
    INCOME,
    EXPENSES;
}
