package com.example.progettomobile.smartwallet.Controller.Fragment.ListsFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Controller.Adapter.ComponentsAdapter;
import com.example.progettomobile.smartwallet.Controller.ListsTransactionsActivity;
import com.example.progettomobile.smartwallet.Controller.SingleListTransactionsActivity;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Lists.Component;
import com.example.progettomobile.smartwallet.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ManageListFragment extends Fragment {

    enum Operation {NAME, COMPONENT}

    private Operation operation = Operation.COMPONENT;
    private TextView listName;
    private Button deleteList;
    private Button changeName;
    private Button addComponent;
    private View mRootView;
    LayoutInflater inflater;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private List<Component> componentList = new ArrayList<>();
    private ComponentsAdapter componentsAdapter;
    private RecyclerView recyclerView;

    public ManageListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_manage_list, container, false);
        this.inflater = inflater;

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        recyclerView = mRootView.findViewById(R.id.recyclerViewListsComponents);
        listName = mRootView.findViewById(R.id.textViewNameLists);
        deleteList = mRootView.findViewById(R.id.buttonDeleteList);
        changeName = mRootView.findViewById(R.id.buttonChangeNameList);
        addComponent = mRootView.findViewById(R.id.buttonAddComponentList);

        setListName();
        mRootView.findViewById(R.id.buttonAddComponentList).setOnClickListener(v -> {
            operation = Operation.COMPONENT;
        });
        mRootView.findViewById(R.id.buttonChangeNameList).setOnClickListener(v -> {
            operation = Operation.NAME;
        });
        deleteList.setOnClickListener(v -> {
            mDatabase.child("lists").child(ListsManager.getID()).removeValue();
            mDatabase.child("users").child(mAuth.getUid()).child("lists").child(ListsManager.getID()).removeValue();
            startActivity(new Intent(getActivity(), ListsTransactionsActivity.class));
        });
        changeName.setOnClickListener(v -> {
            createDialogChangeName();
        });

        addComponent.setOnClickListener(v -> {
            createDialogAddUser();
        });

        componentsAdapter = new ComponentsAdapter(componentList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(componentsAdapter);

        return mRootView;
    }

    public void addComponents(Component component) {
        componentList.add(component);
        componentsAdapter.notifyDataSetChanged();
    }


    private void setListName(){
        listName.setText(ListsManager.getName());
    }

    private void createDialogChangeName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Cambia il nome della lista");

        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.fragment_change_name_list, (ViewGroup) getView(), false);

        final EditText input = (EditText) viewInflated.findViewById(R.id.editTextName);
        builder.setView(viewInflated);


        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if((input.getText().toString() != "") && (input.getText().toString() != null)) {
                    mDatabase.child("lists").child(ListsManager.getID()).child("name").setValue(input.getText().toString());
                    ListsManager.setName(input.getText().toString());
                    Intent intent = new Intent(getActivity(), SingleListTransactionsActivity.class);
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void createDialogAddUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Inserisci un nuovo componente");

        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.fragment_add_component, (ViewGroup) getView(), false);

        final EditText input = (EditText) viewInflated.findViewById(R.id.editTextEmail);
        builder.setView(viewInflated);

        builder.setPositiveButton(R.string.add_component, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Query recentPostsQuery = mDatabase.child("users");
                recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    Boolean added = false;
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            for (DataSnapshot check : postSnapshot.getChildren()) {
                                if (check.getKey().equals("email")) {
                                    if (check.getValue().equals(input.getText().toString())) {
                                        String userId = postSnapshot.getKey();
                                        mDatabase.child("users").child(userId).child("lists").child(ListsManager.getID()).setValue(true);
                                        mDatabase.child("lists").child(ListsManager.getID()).child("people").child(userId).setValue(true);
                                        added = true;
                                        Intent intent = new Intent(getActivity(), SingleListTransactionsActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            }
                        }
                        if(!added){
                            Toast.makeText(getActivity(), "L'email non appartiene a nessun account", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


}
