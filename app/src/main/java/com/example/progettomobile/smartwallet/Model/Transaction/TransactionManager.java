package com.example.progettomobile.smartwallet.Model.Transaction;

import com.example.progettomobile.smartwallet.Model.TypeOperation;

import java.util.Date;

public class TransactionManager {

    private static Transaction currentTransaction;
    private static TypeOperation operation = TypeOperation.FROM_EMPTY;
    private static double amountOfTheMonth = 0;

    public TransactionManager() { }

    public static void initializeManager() {
        currentTransaction = new Transaction();
    }

    public static void clearManager() {
        initializeManager();
    }

    public static boolean checkImportantFields() {
        return TransactionManager.getAmount() > 0.00
                && !TransactionManager.getCategory().equals("")
                && !TransactionManager.getDate().equals("")
                && !TransactionManager.getType().equals("");

    }

    public static Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public static void setAmountOfTheMonth(final double amount) {
        amountOfTheMonth = amount;
    }

    public static double getAmountOfTheMonth() {
        return amountOfTheMonth;
    }

    public static void setOperationType(TypeOperation operationType) {
        operation = operationType;
    }

    public static TypeOperation getOperationType() {
        return operation;
    }

    public static String getID() {
        return currentTransaction.getId();
    }

    public static void setID(final String id) {
        currentTransaction.setId(id);
    }

    public static void setAmount(final double amount) {
        currentTransaction.setAmount(amount);
    }

    public static void setNameShop(final String nameShop) {
        currentTransaction.setShopName(nameShop);
    }

    public static void setNameTransaction(final String name) {
        currentTransaction.setName(name);
    }

    public static void setAddressShop(final String addressShop) {
        currentTransaction.setShopAddress(addressShop);
    }

    public static void setDescription(final String description) {
        currentTransaction.setDescription(description);
    }

    public static void setDate(final String date) {
        currentTransaction.setDate(date);
    }

    public static void setType(final Type type) {
        currentTransaction.setType(type);
    }

    public static void setCategory(final String category) {
        currentTransaction.setCategory(category);
    }

    public static void setPathPhoto(final String pathPhoto) {currentTransaction.setPathPhoto(pathPhoto);}

    public static double getAmount() {
        return currentTransaction.getAmount();
    }

    public static String getNameShop() {
        return currentTransaction.getShopName();
    }

    public static String getNameTransaction() {
        return currentTransaction.getName();
    }

    public static String getAddressShop() {
        return currentTransaction.getShopAddress();
    }

    public static String getDescription() {
        return currentTransaction.getDescription();
    }

    public static String getDate() {
        return currentTransaction.getDate();
    }

    public static String getType() {
        return currentTransaction.getType().toString();
    }

    public static String getCategory() {
        return currentTransaction.getCategory();
    }

    public static String getPathPhoto() {return currentTransaction.getPathPhoto();}
}
