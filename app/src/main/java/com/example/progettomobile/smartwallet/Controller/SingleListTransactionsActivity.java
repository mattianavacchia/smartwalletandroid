package com.example.progettomobile.smartwallet.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.progettomobile.smartwallet.Controller.Adapter.TabAdapter;
import com.example.progettomobile.smartwallet.Controller.Fragment.SelectedAddMethodTransactionDialogFragment;
import com.example.progettomobile.smartwallet.Model.Lists.Component;
import com.example.progettomobile.smartwallet.Model.Lists.ListTransactions;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivitySingleListTransactionsBinding;

import com.example.progettomobile.smartwallet.Controller.Fragment.ListsFragment.ListTransactionsFragment;
import com.example.progettomobile.smartwallet.Controller.Fragment.ListsFragment.ManageListFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class SingleListTransactionsActivity extends BaseActivity implements View.OnClickListener{

    private TabAdapter mTabAdapter;
    private ListTransactionsFragment listTransactionsFragment;
    private ManageListFragment manageListFragment;
    private ActivitySingleListTransactionsBinding binding;
    private DatabaseReference mDatabase;
    private static int menuToChoose = R.menu.drawer_menu_lists;
    private ProgressDialog progressDialog;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_single_list_transactions);
        view = findViewById(R.id.contrainLayoutSingleList);

        ListTransactions list = (ListTransactions) getIntent().getSerializableExtra("list");
        ListsManager.initializeManager();
        ListsManager.setID(list.getId());
        ListsManager.setName(list.getName());

        if(Utility.isNetworkAvailable(this)) {
            mDatabase = FirebaseDatabase.getInstance().getReference();

            mTabAdapter = new TabAdapter(getSupportFragmentManager());

            listTransactionsFragment = new ListTransactionsFragment(this);
            manageListFragment = new ManageListFragment();

            mTabAdapter.addFragment(listTransactionsFragment, ListsManager.getName());
            mTabAdapter.addFragment(manageListFragment, getString(R.string.manage_list));

            binding.viewPager.setAdapter(mTabAdapter);
            binding.tabLayout.setupWithViewPager(binding.viewPager);

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Operazione in corso...");
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();

            getPeople();
            getTransactions();
        } else {
            createSnackbarConnection(view);
        }
    }

    @Override
    public void onBackPressed() {
        if (menuToChoose == R.menu.drawer_menu_lists_selected) {
            menuToChoose = R.menu.drawer_menu_lists;
            invalidateOptionsMenu();
        } else {
            super.onBackPressed();
        }
    }

    private void getPeople() {
        Query recentPostsQuery = mDatabase.child("lists").child(ListsManager.getID()).child("people");
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    getPerson(postSnapshot.getKey());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());

                progressDialog.dismiss();
            }
        });
    }

    private void getPerson(String key) {
        Query recentPostsQuery = mDatabase.child("users").child(key).child("email");
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Component component = new Component();
                component.setEmail(dataSnapshot.getValue().toString());
                component.setId(key);
                manageListFragment.addComponents(component);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void getTransactions() {
        Query recentPostsQuery = mDatabase.child("lists").child(ListsManager.getID()).child("transactions");
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    getTransaction(postSnapshot.getKey());
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());

                progressDialog.dismiss();
            }
        });
    }

    private void getTransaction(String key) {
        Query recentPostsQuery = mDatabase.child("transactions").child(key);
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Transaction transaction = dataSnapshot.getValue(Transaction.class);
                listTransactionsFragment.addTransaction(transaction);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void changeMenu(int idMenu) {
        menuToChoose = idMenu;
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuToChoose = R.menu.drawer_menu_lists;
        invalidateOptionsMenu();

        switch (item.getItemId()) {
            case R.id.add:
                SelectedAddMethodTransactionDialogFragment selectedAddMethodTransactionDialogFragment = new SelectedAddMethodTransactionDialogFragment();
                selectedAddMethodTransactionDialogFragment.show(getSupportFragmentManager(), "selectedAddMethodTransactionDialogFragment");
                return true;
            case R.id.remove:
                mDatabase.child("lists").child(ListsManager.getID()).child("transactions").child(listTransactionsFragment.getIDTransaction());
                mDatabase.child("transactions").child(listTransactionsFragment.getIDTransaction());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(menuToChoose, menu);
        return true;
    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }
}
