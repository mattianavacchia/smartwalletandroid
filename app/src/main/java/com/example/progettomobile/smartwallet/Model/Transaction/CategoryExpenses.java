package com.example.progettomobile.smartwallet.Model.Transaction;

import com.example.progettomobile.smartwallet.R;

public enum CategoryExpenses {

    TAX("TAX", R.drawable.ic_tax),
    FOOD("FOOD", R.drawable.ic_food),
    HOLIDAY("HOLIDAY", R.drawable.ic_holiday),
    GIFT("GIFT", R.drawable.ic_gift),
    PURCHASE("PURCHASE", R.drawable.ic_purchase),
    FAMILY("FAMILY", R.drawable.ic_family),
    HOBBY("HOBBY", R.drawable.ic_hobby),
    TRANSPORT("TRANSPORT", R.drawable.ic_transport),
    OTHER("OTHER", R.drawable.ic_other);

    private final String name;
    private final int path;

    CategoryExpenses(final String n, final int path) {
        this.name = n;
        this.path = path;
    }

    public boolean equals(String otherName, int otherPath) {
        return name.equals(otherName) && (path == otherPath);
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public boolean equalsPath(int otherPath) {
        // (otherPath == null) check is not needed because name.equals(null) returns false
        return path == otherPath;
    }

    public String getName() { return this.name; }

    public int getPath() { return this.path; }

    public String toString() {
        return this.name;
    }
}
