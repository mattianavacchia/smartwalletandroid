package com.example.progettomobile.smartwallet.Controller.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Controller.SingleListTransactionsActivity;
import com.example.progettomobile.smartwallet.Model.Lists.Component;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ComponentsAdapter extends RecyclerView.Adapter<ComponentsAdapter.MyViewHolder> {

    List<Component> components;
    private Context context;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView email;
        public Button buttonRemove;


        public MyViewHolder(View view) {
            super(view);
            email = view.findViewById(R.id.textViewEmailComponent);
            buttonRemove = view.findViewById(R.id.buttonRemoveComponent);
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mAuth = FirebaseAuth.getInstance();

            buttonRemove.setOnClickListener(v->{
                removeComponent(email.getText().toString());
            });
        }
    }

    public ComponentsAdapter(List<Component> c, Context context) {
        this.context = context;
        this.components = c;
    }

    @NonNull
    @Override
    public ComponentsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.component_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.email.setText(components.get(i).getEmail());
    }

    @Override
    public int getItemCount() {
        return components.size();
    }

    public void removeComponent(String email){
        Query recentPostsQuery = mDatabase.child("users");
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot check : postSnapshot.getChildren()) {
                        if (check.getKey().equals("email")) {
                            if (check.getValue().equals(email)) {
                                mDatabase.child("lists").child(ListsManager.getID()).child("people").child(postSnapshot.getKey()).removeValue();
                                mDatabase.child("users").child(postSnapshot.getKey()).child("lists").child(ListsManager.getID()).removeValue();
                                Intent intent = new Intent(context, SingleListTransactionsActivity.class);
                                context.startActivity(intent);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
