package com.example.progettomobile.smartwallet.Controller.Fragment.ListsFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.progettomobile.smartwallet.Controller.Adapter.TransactionsAdapter;
import com.example.progettomobile.smartwallet.Controller.AddTransactionActivity;
import com.example.progettomobile.smartwallet.Controller.Listener.RecyclerTouchListener;
import com.example.progettomobile.smartwallet.Controller.SingleListTransactionsActivity;
import com.example.progettomobile.smartwallet.Controller.TransactionsActivity;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.R;

import java.util.ArrayList;
import java.util.List;

public class ListTransactionsFragment extends Fragment {

    private RecyclerView recyclerView;
    private View mRootView;
    LayoutInflater inflater;
    private TransactionsAdapter transactionsAdapter;
    private List<Transaction> transactionList = new ArrayList<>();
    private SingleListTransactionsActivity activity;
    private int selectedPosition;

    public ListTransactionsFragment() { }

    @SuppressLint("ValidFragment")
    public ListTransactionsFragment(SingleListTransactionsActivity activity) { this.activity = activity; }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list_transactions, container, false);
        this.inflater = inflater;

        recyclerView = mRootView.findViewById(R.id.recyclerViewListFragment);

        transactionsAdapter = new TransactionsAdapter(transactionList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(transactionsAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TransactionManager.setOperationType(TypeOperation.MODIFY);
                Intent intent = new Intent(getActivity(), AddTransactionActivity.class);
                intent.putExtra("operation", "MODIFY");
                intent.putExtra("transaction", transactionList.get(position));
                startActivity(intent);

                ListsManager.setStateDoing();
            }

            @Override
            public void onLongClick(View view, int position) {
                activity.changeMenu(R.menu.drawer_menu_lists_selected);
                Vibrator vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(100);

                selectedPosition = position;
            }
        }));

        return mRootView;
    }

    public void addTransaction(Transaction transaction) {
        transactionList.add(transaction);
        transactionsAdapter.notifyDataSetChanged();
    }

    public String getIDTransaction() {
        return transactionList.get(selectedPosition).getId();
    }
}
