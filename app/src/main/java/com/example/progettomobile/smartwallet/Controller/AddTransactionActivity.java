package com.example.progettomobile.smartwallet.Controller;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Controller.Adapter.TabAdapter;
import com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction.DescriptionFragment;
import com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction.EndFragment;
import com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction.PhotoFragment;
import com.example.progettomobile.smartwallet.Model.OcrManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivityAddTransactionBinding;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import java.io.IOException;

public class AddTransactionActivity extends AppCompatActivity {

    private Bitmap bitmapSelectedImage;
    private ActivityAddTransactionBinding binding;
    private TabAdapter mTabAdapter;
    private DescriptionFragment descriptionFragment;
    private PhotoFragment photoFragment;
    private EndFragment endFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_transaction);

        // check which action needs to be performed
        String action = getIntent().getStringExtra("operation");

        TransactionManager.initializeManager();
        if (action.equals("OCR_IMAGE")) {
            performOCR();
        } else {
            if (action.equals("MODIFY")) {
                Transaction transaction = (Transaction) getIntent().getSerializableExtra("transaction");
                populateWithTransaction(transaction);
            }
        }

        mTabAdapter = new TabAdapter(getSupportFragmentManager());

        descriptionFragment = new DescriptionFragment();
        photoFragment = new PhotoFragment();
        endFragment = new EndFragment();

        mTabAdapter.addFragment(descriptionFragment, getString(R.string.name_first_tab_add_transaction));
        mTabAdapter.addFragment(photoFragment, getString(R.string.name_second_tab_add_transaction));
        mTabAdapter.addFragment(endFragment, getString(R.string.name_third_tab_add_transaction));

        binding.viewPager.setAdapter(mTabAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    descriptionFragment.getContentFromComponents();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    descriptionFragment.getContentFromComponents();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    descriptionFragment.getContentFromComponents();
                }
            }
        });
    }

    private void performOCR() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Operazione in corso...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        String source = getIntent().getStringExtra("source");
        if (source.equals("library")) {
            try {
                bitmapSelectedImage =  MediaStore.Images.Media.getBitmap(this.getContentResolver(), getIntent().getParcelableExtra("image_selected"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Bundle extras = getIntent().getExtras();
            byte[] byteArray = extras.getByteArray("image_picture");

            bitmapSelectedImage = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }

        if (bitmapSelectedImage != null) {
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmapSelectedImage);
            FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                    .getOnDeviceTextRecognizer();

            detector.processImage(image)
                    .addOnSuccessListener(firebaseVisionText -> {
                        TransactionManager.setNameShop(OcrManager.getName(firebaseVisionText));
                        TransactionManager.setAddressShop(OcrManager.getAddress(firebaseVisionText));
                        TransactionManager.setAmount(OcrManager.getTotal(firebaseVisionText));
                        TransactionManager.setDescription(OcrManager.getDetails(firebaseVisionText));
                        TransactionManager.setDate(OcrManager.getDate(firebaseVisionText).toString());
                        if (source.equals("library"))
                            TransactionManager.setPathPhoto(getIntent().getParcelableExtra("image_selected").toString());

                        this.descriptionFragment.populateComponents();
                        this.photoFragment.setImage();
                        progressDialog.dismiss();
                    })
                    .addOnFailureListener(e -> Toast.makeText(getApplicationContext(), getString(R.string.error_ocr), Toast.LENGTH_SHORT).show());
        } else  {
            Toast.makeText(getApplicationContext(), getString(R.string.error_ocr), Toast.LENGTH_SHORT).show();
        }
    }

    private void populateWithTransaction(final Transaction transaction) {
        TransactionManager.setID(transaction.getId());
        TransactionManager.setAmount(transaction.getAmount());
        TransactionManager.setType(transaction.getType());
        TransactionManager.setNameTransaction(transaction.getName());
        TransactionManager.setNameShop(transaction.getShopName());
        TransactionManager.setAddressShop(transaction.getShopAddress());
        TransactionManager.setCategory(transaction.getCategory());
        TransactionManager.setDate(transaction.getDate());
        TransactionManager.setDescription(transaction.getDescription());
        TransactionManager.setPathPhoto(transaction.getPathPhoto());
    }
}
