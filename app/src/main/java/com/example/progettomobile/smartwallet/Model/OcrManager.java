package com.example.progettomobile.smartwallet.Model;

import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.firebase.ml.vision.text.FirebaseVisionText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class OcrManager{

    private static Double totalAmount = 0.00;
    private static Date date = new Date();
    private static String shopName = "";
    private static String shopAddress = "";
    private static String details = "";
    private static List<SimpleDateFormat> dateFormats  = new ArrayList<SimpleDateFormat>() {
        {
            add(new SimpleDateFormat("dd-MM-yyyy"));
            add(new SimpleDateFormat("dd/MM/yyyy"));
            add(new SimpleDateFormat("yyyy-MM-dd"));
            add(new SimpleDateFormat("yyyy/MM/dd"));
            add(new SimpleDateFormat("MM-dd-yyyy"));
            add(new SimpleDateFormat("MM/dd/yyyy"));
        }
    };

    public static Double getTotal(FirebaseVisionText firebaseVisionText){
        Integer top = 0;
        Integer bottomRight = 10000;
        OcrManager.totalAmount = 0.00;
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("TOTALE") || elementText.equalsIgnoreCase("IMPORTO")){
                        top = lineFrame.top;
                    }
                }
            }
        }
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    Rect elementFrame = element.getBoundingBox();
                    if(blockFrame.top > top -20 && blockFrame.top < top +20){

                        if (elementFrame.bottom < bottomRight && (!elementText.trim().matches(".*\\d+.*") || elementText.trim().contains("/") || elementText.trim().contains("-"))){
                            OcrManager.totalAmount = 0.00;
                        } else if(elementFrame.bottom < bottomRight && elementText.trim().matches(".*\\d+.*")){
                            OcrManager.totalAmount = Double.parseDouble(elementText.replace(",","."));
                            bottomRight = lineFrame.bottom;
                        } 
                    }
                }
            }
        }
        if (totalAmount.equals(0.00))
            return 0.00;
        else
            return totalAmount;
    }

    public static Date getDate(FirebaseVisionText firebaseVisionText){
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    for (SimpleDateFormat format : dateFormats) {
                        try {
                            format.setLenient(false);
                            date = format.parse(elementText);
                        } catch (ParseException e) {
                            Log.e("OcrManagerERROR", e.getMessage());
                        }
                    }
                }
            }
        }
        return date;
    }

    public static String getName(FirebaseVisionText firebaseVisionText){
        Integer top = 10000;
        Integer firstBlock = 10000;
        OcrManager.shopName = "";
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if (blockFrame.top < firstBlock){
                        firstBlock = blockFrame.top;
                    }
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < top){
                            top = blockFrame.top;
                        }
                    }

                }
            }
        }
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                String lineText = line.getText();
                if ((lineFrame.top < top +30) && lineText != shopAddress){
                    OcrManager.shopName = lineText;
                }

            }
        }

        if(shopName.equals("")){
            for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
                for (FirebaseVisionText.Line line: block.getLines()) {
                    Rect lineFrame = line.getBoundingBox();
                    String lineText = line.getText();
                    if ((lineFrame.top < firstBlock +30)){
                        OcrManager.shopName = lineText;
                    }

                }
            }
        }

        return OcrManager.shopName;
    }

    public static String getAddress(FirebaseVisionText firebaseVisionText){
        Integer top = 10000;
        OcrManager.shopAddress = "";
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                String lineText = line.getText();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < top){
                            top = blockFrame.top;
                            OcrManager.shopAddress = lineText;
                        }
                    }
                }
            }
        }
        return OcrManager.shopAddress;
    }

    public static String getDetails(FirebaseVisionText firebaseVisionText){
        Integer topAddress = 10000;
        Integer bottomAddress = 10000;
        Integer topTot = 10000;
        details = "";
        ConcurrentHashMap<Integer,String> temp = new ConcurrentHashMap<>();
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < topAddress){
                            topAddress = blockFrame.top;
                            bottomAddress = blockFrame.bottom;
                        }
                    }
                    if(elementText.equalsIgnoreCase("TOTALE") || elementText.equalsIgnoreCase("IMPORTO")){
                        topTot = blockFrame.top;

                    }
                }
            }
        }

        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                String lineText = line.getText();
                if (blockFrame.top > bottomAddress && blockFrame.bottom < topTot) {
                    Integer currentLine = lineFrame.bottom;
                    temp.put(currentLine,lineText);
                    for (Integer i : temp.keySet()) {
                        if (currentLine > i - 10 && currentLine < i + 10) {
                            String tmp = "";
                            tmp = temp.get(i);
                            if(tmp == null){
                                tmp = "";
                            } else if(!tmp.contains(lineText)){
                                tmp += " "+lineText;
                                temp.remove(i);
                                temp.remove(currentLine);
                                if(!temp.contains(lineText)){
                                    temp.put(i, tmp);
                                }
                            }
                        }
                    }

                }
            }

        }

        for(Integer i : temp.keySet()){
            details += temp.get(i)+"\n";
        }

        return details;
    }

}