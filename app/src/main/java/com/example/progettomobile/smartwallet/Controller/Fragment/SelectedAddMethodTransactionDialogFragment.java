package com.example.progettomobile.smartwallet.Controller.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.example.progettomobile.smartwallet.Controller.AddTransactionActivity;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.R;

public class SelectedAddMethodTransactionDialogFragment extends AppCompatDialogFragment {

    public static final int TAKEFROMCAMERACODE = 0;
    public static final int TAKEFROMGALLERYCODE = 1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_select_add_method, null);

        builder.setView(view)
                .setTitle("Lista Amici")
                .setNegativeButton("ANNULLA", (dialogInterface, i) -> {
                });

        view.findViewById(R.id.buttonFromLibraryFragmentAddMethod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , TAKEFROMGALLERYCODE);

                ListsManager.setStateDoing();
            }
        });
        view.findViewById(R.id.buttonFromCameraFragmentAddMethod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, TAKEFROMCAMERACODE);

                ListsManager.setStateDoing();
            }
        });
        view.findViewById(R.id.buttonByHandFragmentAddMethod).setOnClickListener(v -> {
            TransactionManager.setOperationType(TypeOperation.FROM_EMPTY);
            Intent intent = new Intent(getActivity(), AddTransactionActivity.class);
            intent.putExtra("operation", "NOTHING");
            startActivity(intent);

            ListsManager.setStateDoing();
        });

        return builder.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data.getData() != null) {
            TransactionManager.setOperationType(TypeOperation.OCR);
            Uri uriSelectedImage = data.getData();

            if (uriSelectedImage != null) {
                Intent intent = new Intent(getActivity(), AddTransactionActivity.class);
                intent.putExtra("operation", "OCR_IMAGE");
                intent.putExtra("source", "library");
                intent.putExtra("image_selected", uriSelectedImage);
                startActivity(intent);
            }
        } else {
            //Snackbar.make(binding.coordinatorLayoutTakePhoto, getString(R.string.error_no_pick_photo), Toast.LENGTH_SHORT).show();
        }
    }
}
