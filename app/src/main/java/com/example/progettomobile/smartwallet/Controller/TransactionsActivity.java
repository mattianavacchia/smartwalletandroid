package com.example.progettomobile.smartwallet.Controller;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.progettomobile.smartwallet.Controller.Adapter.TransactionsAdapter;
import com.example.progettomobile.smartwallet.Controller.Listener.RecyclerTouchListener;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Type;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivityMainBinding;
import com.example.progettomobile.smartwallet.databinding.ActivityTransactionsBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

public class TransactionsActivity extends BaseActivity implements View.OnClickListener{

    private ActivityTransactionsBinding binding;
    private ActionBarDrawerToggle toggle;
    private Calendar date;
    private TransactionsAdapter transactionsAdapter;
    private DatabaseReference mDatabase;
    private List<Transaction> transactionList = new ArrayList<>();
    private FirebaseAuth mAuth;
    private int menuToChoose = R.menu.drawer_menu;
    private View selectedViewList;
    private int positionSelectedTransaction = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_transactions);
        date = Calendar.getInstance();
        date.setTime(new Date());
        transactionsAdapter = new TransactionsAdapter(transactionList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerView.setAdapter(transactionsAdapter);

        if(Utility.isNetworkAvailable(this)) {
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();

            binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), binding.recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    // go to AddTransaction
                    TransactionManager.setOperationType(TypeOperation.MODIFY);
                    Intent intent = new Intent(TransactionsActivity.this, AddTransactionActivity.class);
                    intent.putExtra("operation", "MODIFY");
                    intent.putExtra("transaction", transactionList.get(position));
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {
                    selectedViewList = view;
                    positionSelectedTransaction = position;
                    menuToChoose = R.menu.drawer_menu_transactions;
                    invalidateOptionsMenu();

                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                }
            }));
        } else {
            createSnackbarConnection(selectedViewList);
        }
    }

    @Override
    public void onBackPressed() {
        if (menuToChoose == R.menu.drawer_menu_transactions) {
            menuToChoose = R.menu.drawer_menu;
            invalidateOptionsMenu();
        } else {
            super.onBackPressed();
        }
    }

    public void goToStatistics(View view) {
        String monthYear = Utility.getMonthYear(Utility.getDateFromDatePickerD(binding.datePickerTransactions));
        Intent i = new Intent(this, StatisticsActivity.class);
        i.putExtra("monthYear", monthYear);
        startActivity(i);
    }

    public void goToMap(View view) {
        if (transactionList.size() > 0) {
            Intent i = new Intent(this, MapActivity.class);
            i.putExtra("transactions", (Serializable) transactionList);
            startActivity(i);
        } else
            Snackbar.make(binding.drawerlayout, "Non ci sono transazioni da visualizzare sulla mappa.", Snackbar.LENGTH_SHORT).show();
    }

    public void showTransactions(View view) {
        transactionList.clear();
        transactionsAdapter.notifyDataSetChanged();
        getDataFromRealTimeDatabase();
    }

    private void getDataFromRealTimeDatabase() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Operazione in corso...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid()).child(Utility.getMonthYear(Utility.getDateFromDatePickerD(binding.datePickerTransactions)));
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    if (!postSnapshot.getKey().equals("amount"))
                        getTransaction(postSnapshot.getKey());
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());

                progressDialog.dismiss();
            }
        });
    }

    private void getTransaction(String key) {
        Query recentPostsQuery = mDatabase.child("transactions").child(key);
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Transaction transaction = dataSnapshot.getValue(Transaction.class);
                transactionList.add(transaction);
                transactionsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuToChoose = R.menu.drawer_menu_transactions;
        invalidateOptionsMenu();

        switch (item.getItemId()) {
            case R.id.edit:
                TransactionManager.setOperationType(TypeOperation.MODIFY);
                Intent intent = new Intent(TransactionsActivity.this, AddTransactionActivity.class);
                intent.putExtra("operation", "MODIFY");
                intent.putExtra("transaction", transactionList.get(positionSelectedTransaction));
                startActivity(intent);
                return true;
            case R.id.remove:
                double amount = TransactionManager.getAmountOfTheMonth();
                if(transactionList.get(positionSelectedTransaction).getType() == Type.EXPENSES){
                    amount += transactionList.get(positionSelectedTransaction).getAmount();
                } else {
                    amount -= transactionList.get(positionSelectedTransaction).getAmount();
                }
                mDatabase.child("users").child(mAuth.getUid()).child(Utility.getMonthYear(Utility.getDateFromDatePickerD(binding.datePickerTransactions))).child("amount").setValue(amount);
                mDatabase.child("transactions").child(transactionList.get(positionSelectedTransaction).getId()).removeValue();
                mDatabase.child("users").child(mAuth.getUid()).child(Utility.getMonthYear(Utility.getDateFromDatePickerD(binding.datePickerTransactions))).child(transactionList.get(positionSelectedTransaction).getId()).removeValue();
                transactionList.remove(positionSelectedTransaction);
                transactionsAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(menuToChoose, menu);
        return true;
    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

}
