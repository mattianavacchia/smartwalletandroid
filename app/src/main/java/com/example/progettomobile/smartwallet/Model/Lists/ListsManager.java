package com.example.progettomobile.smartwallet.Model.Lists;

import java.util.UUID;

public class ListsManager {

    enum State { NONE, DOING }

    private static State state = State.NONE;

    private static ListTransactions currentListTransactions;

    private ListsManager() { }

    public static void initializeManager() {
        currentListTransactions = new ListTransactions();
    }

    public static void clearManager() {
        initializeManager();
    }

    public static void setStateDoing() {
        state = State.DOING;
    }

    public static void setStateNone() {
        state = State.NONE;
    }

    public static boolean isDoing() {
        return state == State.DOING;
    }

    public static ListTransactions getCurrentListTransactions() {
        return currentListTransactions;
    }

    public static void setName(String name) {
        currentListTransactions.setName(name);
    }

    public static String getName() {
        return currentListTransactions.getName();
    }

    public static void setID(String id) {
        currentListTransactions.setId(id);
    }

    public static String getID() {
        return currentListTransactions.getId();
    }

    public static String getNewID() {
        return UUID.randomUUID().toString();
    }

}
