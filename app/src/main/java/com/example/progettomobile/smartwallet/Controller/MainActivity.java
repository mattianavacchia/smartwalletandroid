package com.example.progettomobile.smartwallet.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Controller.Adapter.TransactionsAdapter;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivityMainBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

public class MainActivity extends BaseActivity implements View.OnClickListener{

    private ActivityMainBinding binding;
    private TransactionsAdapter transactionsAdapter;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private View view;
    private final int limit = 3;

    private List<com.example.progettomobile.smartwallet.Model.Transaction.Transaction> transactionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view = findViewById(R.id.scrollViewMain);

        if(Utility.isNetworkAvailable(this)) {

            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();

            binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

            binding.textViewMonthOf.setText(getResources().getString(R.string.month_of, getMonthName()));

            transactionsAdapter = new TransactionsAdapter(transactionList, getApplicationContext());

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            binding.recyclerView.setLayoutManager(mLayoutManager);
            binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
            binding.recyclerView.setAdapter(transactionsAdapter);

            getDataFromRealTimeDatabase();
        } else {
            createSnackbarConnection(view);
        }
    }

    public void goToTransactions(View view) {
        startActivity(new Intent(this, TransactionsActivity.class));
    }

    private void getDataFromRealTimeDatabase() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Operazione in corso...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        if (Utility.isNetworkAvailable(this)) {
            Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid()).child(Utility.getMonthYear());
            recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot.getKey() != null && postSnapshot.getKey().equals("amount")) {
                            binding.textViewAmountCurrentMonth.setText(String.format("%.2f", Double.valueOf(postSnapshot.getValue().toString())) + " €");
                            TransactionManager.setAmountOfTheMonth(Double.valueOf(postSnapshot.getValue().toString()));
                            if (TransactionManager.getAmountOfTheMonth() > 0)
                                binding.linearLayoutTotaleAmountMain.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorButtonContinueTakePhoto)));
                            else
                                binding.linearLayoutTotaleAmountMain.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorRed)));
                        } else {
                            getTransaction(postSnapshot.getKey());
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());

                    progressDialog.dismiss();
                }
            });
        } else {
            createSnackbarConnection(view);
        }
    }

    private void getTransaction(String key) {
        if(Utility.isNetworkAvailable(this)) {
            Query recentPostsQuery = mDatabase.child("transactions").child(key);
            recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Transaction transaction = dataSnapshot.getValue(Transaction.class);
                    if(transactionList.size() < limit) {
                        transactionList.add(transaction);
                        
                        transactionsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        } else {
            createSnackbarConnection(view);
        }
    }

    private String getMonthName() {
        Formatter fmt;
        Calendar cal = Calendar.getInstance();
        fmt = new Formatter();

        return fmt.format("%tB", cal).toString();
    }

    public void insertByHand(View view) {
        TransactionManager.setOperationType(TypeOperation.FROM_EMPTY);
        Intent intent = new Intent(this, AddTransactionActivity.class);
        intent.putExtra("operation", "NOTHING");
        startActivity(intent);
    }

    public void insertWithOCR(View view) {
        TransactionManager.setOperationType(TypeOperation.OCR);
        startActivity(new Intent(this, TakePhotoActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }
}
