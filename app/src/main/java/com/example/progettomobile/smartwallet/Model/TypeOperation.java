package com.example.progettomobile.smartwallet.Model;

public enum TypeOperation {
    FROM_EMPTY,
    MODIFY,
    OCR
}
