package com.example.progettomobile.smartwallet.Model.Transaction;

import android.os.Parcelable;
import android.widget.DatePicker;

import com.example.progettomobile.smartwallet.Model.Utility;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Transaction implements Serializable {
    private String id;
    private Type type;
    private String name;
    private String shopName;
    private String shopAddress;
    private String category;
    private String date;
    private String description;
    private double amount;
    private String pathPhoto;

    public Transaction() {
        this.id = Utility.getIDForTransaction();
        this.type = Type.EXPENSES;
        this.name = "";
        this.shopName = "";
        this.shopAddress = "";
        this.category = "";
        this.date = Calendar.getInstance().getTime().toString();
        this.description = "";
        this.amount = 0.00;
        this.pathPhoto = "";
    }

    public Transaction(String id, Type type, String name, String shopName, String shopAddress, String category, String date, String description, double amount) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.category = category;
        this.date = date;
        this.description = description;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPathPhoto() { return pathPhoto; }

    public void setPathPhoto(final String pathPhoto) { this.pathPhoto = pathPhoto; }
}
