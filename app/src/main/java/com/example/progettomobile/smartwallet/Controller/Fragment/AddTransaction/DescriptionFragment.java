package com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Model.Transaction.CategoryExpenses;
import com.example.progettomobile.smartwallet.Model.Transaction.CategoryIncome;
import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.Model.Transaction.Type;
import com.example.progettomobile.smartwallet.Model.TypeOperation;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class DescriptionFragment extends Fragment {

    View mRootView;
    LayoutInflater inflater;
    Type currentType = Type.EXPENSES;
    Button mButtonIncome, mButtonExpenses;
    CoordinatorLayout mCoordinatorLayoutDescription;
    GridLayout mLayoutAddTransactionCategory;
    TextView mTextViewCurrency;
    EditText mEditTextAmount, mEditTextNameShop, mEditTextNameTransaction, mEditTextAddressShop, mEditTextDescription;
    DatePicker mDatePicker;

    public DescriptionFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_description, container, false);
        this.inflater = inflater;

        /* BINDING WITH GRAPHICS COMPONENT */
        mButtonIncome = mRootView.findViewById(R.id.buttonIncome);
        mButtonExpenses = mRootView.findViewById(R.id.buttonExpenses);
        mCoordinatorLayoutDescription = mRootView.findViewById(R.id.coordinatorLayoutDescription);
        mLayoutAddTransactionCategory = mRootView.findViewById(R.id.layoutAddTransactionCategory);
        mEditTextAmount = mRootView.findViewById(R.id.editTextAmount);
        mEditTextNameShop = mRootView.findViewById(R.id.editTextNameShop);
        mEditTextNameTransaction = mRootView.findViewById(R.id.editTextNameTransaction);
        mEditTextAddressShop = mRootView.findViewById(R.id.editTextAddressShop);
        mEditTextDescription = mRootView.findViewById(R.id.editTextDescription);
        mDatePicker = mRootView.findViewById(R.id.datePicker);
        mTextViewCurrency = mRootView.findViewById(R.id.textViewCurrency);

        handleFocusEditText();

        generateCategoriesButtons(inflater);

        /* OTHER */
        mButtonIncome.setOnClickListener(v -> {
            changeToIncome();
        });
        mButtonExpenses.setOnClickListener(v -> {
            changeToExpenses();
        });

        if (TransactionManager.getOperationType() != TypeOperation.OCR) {
            populateComponents();
        }
        for (int i= 0; i < mLayoutAddTransactionCategory.getChildCount(); i++){
            LinearLayout itemGrid = (LinearLayout) mLayoutAddTransactionCategory.getChildAt(i);
            TextView t = (TextView) itemGrid.getChildAt(1);
            itemGrid.setOnClickListener(view -> {
                TransactionManager.setCategory(t.getText().toString());
                Snackbar.make(mCoordinatorLayoutDescription, "Selezionata " + t.getText().toString() + " selezionata.", Snackbar.LENGTH_SHORT).show();
            });
        }

        return mRootView;
    }

    private void changeToIncome() {
        mButtonIncome.setBackground(getResources().getDrawable(R.drawable.button_style_income_selected));
        mButtonIncome.setTextColor(getResources().getColor(R.color.white));
        mButtonExpenses.setBackground(getResources().getDrawable(R.drawable.button_style_expenses_not_selected));
        mButtonExpenses.setTextColor(getResources().getColor(R.color.colorRed));
        mEditTextAmount.setTextColor(getResources().getColor(R.color.colorButtonContinueTakePhoto));
        mTextViewCurrency.setTextColor(getResources().getColor(R.color.colorButtonContinueTakePhoto));
        mEditTextDescription.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        mEditTextNameShop.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        mEditTextNameTransaction.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        mEditTextAddressShop.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        mEditTextDescription.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        mEditTextAmount.getBackground().setTint(getResources().getColor(R.color.colorGreenDark));
        currentType = Type.INCOME;
        generateCategoriesButtons(this.inflater);
        TransactionManager.setType(currentType);
    }

    private void changeToExpenses() {
        mButtonExpenses.setBackground(getResources().getDrawable(R.drawable.button_style_expenses_selected));
        mButtonExpenses.setTextColor(getResources().getColor(R.color.white));
        mButtonIncome.setBackground(getResources().getDrawable(R.drawable.button_style_income_not_selected));
        mButtonIncome.setTextColor(getResources().getColor(R.color.colorButtonContinueTakePhoto));
        mEditTextAmount.setTextColor(getResources().getColor(R.color.colorRed));
        mTextViewCurrency.setTextColor(getResources().getColor(R.color.colorRed));
        mEditTextDescription.getBackground().setTint(getResources().getColor(R.color.colorRed));
        mEditTextNameShop.getBackground().setTint(getResources().getColor(R.color.colorRed));
        mEditTextNameTransaction.getBackground().setTint(getResources().getColor(R.color.colorRed));
        mEditTextAddressShop.getBackground().setTint(getResources().getColor(R.color.colorRed));
        mEditTextDescription.getBackground().setTint(getResources().getColor(R.color.colorRed));
        mEditTextAmount.getBackground().setTint(getResources().getColor(R.color.colorRed));
        currentType = Type.EXPENSES;
        generateCategoriesButtons(this.inflater);
        TransactionManager.setType(currentType);
    }

    public void getContentFromComponents() {
        TransactionManager.setAmount(Double.valueOf(mEditTextAmount.getText().toString()));
        TransactionManager.setNameShop(mEditTextNameShop.getText().toString());
        TransactionManager.setNameTransaction(mEditTextNameTransaction.getText().toString());
        TransactionManager.setAddressShop(mEditTextAddressShop.getText().toString());
        TransactionManager.setDescription(mEditTextDescription.getText().toString());
        TransactionManager.setDate(Utility.getDateFromDatePicker(mDatePicker).toString());
    }

    public void populateComponents() {
        if (TransactionManager.getType().equals(Type.INCOME.toString())) {
            changeToIncome();
        } else {
            changeToExpenses();
        }
        mEditTextNameTransaction.setText(TransactionManager.getNameTransaction());
        mEditTextNameShop.setText(TransactionManager.getNameShop());
        mEditTextAddressShop.setText(TransactionManager.getAddressShop());
        mEditTextDescription.setText(TransactionManager.getDescription());
        mEditTextAmount.setText(Double.toString(TransactionManager.getAmount()));
        //int[] date = Utility.getYearMonthDay(TransactionManager.getDate());
        //mDatePicker.updateDate(date[0], date[1], date[2]);
    }

    private void handleFocusEditText() {
        mEditTextAmount.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                if (!mEditTextAmount.getText().toString().equals(""))
                    TransactionManager.setAmount(Double.valueOf(mEditTextAmount.getText().toString()));
            }
        });
        mEditTextNameShop.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                TransactionManager.setNameShop(mEditTextNameShop.getText().toString());
            }
        });
        mEditTextNameTransaction.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                TransactionManager.setNameTransaction(mEditTextNameTransaction.getText().toString());
            }
        });
        mEditTextAddressShop.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                TransactionManager.setAddressShop(mEditTextAddressShop.getText().toString());
            }
        });
        mEditTextDescription.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                TransactionManager.setDescription(mEditTextDescription.getText().toString());
                TransactionManager.setDate(Utility.getDateFromDatePicker(mDatePicker).toString());
            }
        });
    }

    private void generateCategoriesButtons(LayoutInflater inflater) {
        mLayoutAddTransactionCategory.removeAllViews();
        if (currentType == Type.EXPENSES) {
            for (CategoryExpenses c: CategoryExpenses.values()) {
                LinearLayout linearLayout = new LinearLayout(inflater.getContext());
                linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                linearLayout.setPadding(0, 8, 0, 8);
                linearLayout.setMinimumWidth(230);

                CircleImageView imageView = new CircleImageView(linearLayout.getContext());
                imageView.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
                if (c.getPath() != -1)
                    imageView.setImageDrawable(getResources().getDrawable(c.getPath()));
                imageView.setBorderWidth(15);
                imageView.setBorderColor(getResources().getColor(R.color.colorRed));
                imageView.setBackground(getResources().getDrawable(R.drawable.layout_expenses));
                TextView textView = new TextView(linearLayout.getContext());
                textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setText(c.getName());
                textView.setTextColor(getResources().getColor(R.color.colorRed));
                textView.setPadding(0, 0, 0, 64);

                linearLayout.addView(imageView);
                linearLayout.addView(textView);

                mLayoutAddTransactionCategory.addView(linearLayout);
                mLayoutAddTransactionCategory.setBackground(getResources().getDrawable(R.drawable.grid_categories_background_expenses));
            }
        } else {
            for (CategoryIncome c: CategoryIncome.values()) {
                LinearLayout linearLayout = new LinearLayout(inflater.getContext());
                linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                linearLayout.setPadding(0, 8, 0, 8);
                linearLayout.setMinimumWidth(240);

                CircleImageView imageView = new CircleImageView(linearLayout.getContext());
                imageView.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
                if (c.getPath() != -1)
                    imageView.setImageDrawable(getResources().getDrawable(c.getPath()));
                imageView.setBorderWidth(15);
                imageView.setBorderColor(getResources().getColor(R.color.colorGreenDark));
                imageView.setBackground(getResources().getDrawable(R.drawable.layout_income));
                TextView textView = new TextView(linearLayout.getContext());
                textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setText(c.getName());
                textView.setTextColor(getResources().getColor(R.color.colorGreenDark));
                textView.setPadding(0, 0, 0, 64);

                linearLayout.addView(imageView);
                linearLayout.addView(textView);

                mLayoutAddTransactionCategory.addView(linearLayout);
                mLayoutAddTransactionCategory.setBackground(getResources().getDrawable(R.drawable.grid_categories_background_income));
            }
        }
        for (int i= 0; i < mLayoutAddTransactionCategory.getChildCount(); i++){
            LinearLayout itemGrid = (LinearLayout) mLayoutAddTransactionCategory.getChildAt(i);
            TextView t = (TextView) itemGrid.getChildAt(1);
            itemGrid.setOnClickListener(view -> {
                TransactionManager.setCategory(t.getText().toString());
                Snackbar.make(mCoordinatorLayoutDescription, "Selezionata " + t.getText().toString() + " selezionata.", Snackbar.LENGTH_SHORT).show();
            });
        }
    }
}
