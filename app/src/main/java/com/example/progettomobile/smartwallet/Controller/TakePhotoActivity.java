package com.example.progettomobile.smartwallet.Controller;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivityTakePhotoBinding;

import java.io.ByteArrayOutputStream;

public class TakePhotoActivity extends AppCompatActivity {

    public static final int TAKEFROMCAMERACODE = 0;
    public static final int TAKEFROMGALLERYCODE = 1;
    private Uri uriSelectedImage;
    private ActivityTakePhotoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_take_photo);
    }

    public void takePhotoWithCamera(View view) {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, TAKEFROMCAMERACODE);
    }

    public void takePhotoFromGallery(View view) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , TAKEFROMGALLERYCODE);
    }

    public void continueWithPhoto(View view) {
        Intent intent = new Intent(this, AddTransactionActivity.class);
        intent.putExtra("operation", "OCR_IMAGE");
        if (uriSelectedImage == null) {
            intent.putExtra("source", "camera");
            binding.imageSelected.setDrawingCacheEnabled(true);
            Bitmap bmp = binding.imageSelected.getDrawingCache();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            intent.putExtra("image_picture", byteArray);
        }
        else {
            intent.putExtra("source", "library");
            intent.putExtra("image_selected", uriSelectedImage);
        }
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle InstanceState) {
        super.onSaveInstanceState(InstanceState);
        InstanceState.clear();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (imageReturnedIntent != null) {
            uriSelectedImage = imageReturnedIntent.getData();
            if (uriSelectedImage == null) {
                Bitmap photo = (Bitmap) imageReturnedIntent.getExtras().get("data");
                binding.imageSelected.setImageBitmap(photo);
            } else {
                binding.imageSelected.setImageURI(uriSelectedImage);
            }
            binding.imageSelected.setVisibility(View.VISIBLE);
            binding.buttonTakePhotoContinue.setVisibility(View.VISIBLE);
        } else {
            Snackbar.make(binding.coordinatorLayoutTakePhoto, getString(R.string.error_no_pick_photo), Toast.LENGTH_SHORT).show();
        }
    }
}
