package com.example.progettomobile.smartwallet.Controller.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.progettomobile.smartwallet.Model.Transaction.CategoryExpenses;
import com.example.progettomobile.smartwallet.Model.Transaction.CategoryIncome;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.Model.Transaction.Type;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;

import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.MyViewHolder> {

    private Context context;
    private List<Transaction> transactionList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public TextView name, date, amount;
        public ImageView category;

        public MyViewHolder(View view) {
            super(view);
            layout = view.findViewById(R.id.linearLayoutTransactionListRow);
            name = view.findViewById(R.id.textViewNameList);
            date = view.findViewById(R.id.textViewDateList);
            amount = view.findViewById(R.id.textViewAmountList);
            category = view.findViewById(R.id.imageViewCategoryList);
        }
    }

    public TransactionsAdapter(List<Transaction> transactionsList, Context context) {
        this.context = context;
        this.transactionList = transactionsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.transaction_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Transaction transaction = transactionList.get(i);
        if (transaction.getType() == Type.INCOME)
            myViewHolder.layout.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorButtonContinueTakePhoto)));
        else
            myViewHolder.layout.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorRed)));
        myViewHolder.name.setText(transaction.getName());
        myViewHolder.date.setText(transaction.getDate());
        myViewHolder.amount.setText(String.format("%.2f", transaction.getAmount()) + " €");
        if (transaction.getType() == Type.INCOME) {
            myViewHolder.category.setImageDrawable(
                    context.getResources()
                            .getDrawable(CategoryIncome.valueOf(transaction.getCategory()).getPath()));
            //DrawableCompat.setTint(myViewHolder.category.getDrawable(), ContextCompat.getColor(this.context, R.color.black));
        } else {
            myViewHolder.category.setImageDrawable(
                    context.getResources()
                            .getDrawable(CategoryExpenses.valueOf(transaction.getCategory()).getPath()));
            //DrawableCompat.setTint(myViewHolder.category.getDrawable(), ContextCompat.getColor(this.context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }
}
