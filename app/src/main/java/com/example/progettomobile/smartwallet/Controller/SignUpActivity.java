package com.example.progettomobile.smartwallet.Controller;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivitySignupBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.progettomobile.smartwallet.Model.Utility.messageErrorFromCode;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SmartWalletTAG";
    private FirebaseAuth mAuth;
    private  ActivitySignupBinding binding;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);


        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        binding.buttonRegister.setOnClickListener((View v) -> {
            mAuth.createUserWithEmailAndPassword(binding.editTextEmailSignUp.getText().toString(), binding.editTextPasswordSignUp.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                //Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                mDatabase.child("users").child(user.getUid()).child("email").setValue(user.getEmail());
                                notifyUser(getString(R.string.registration_successful));
                                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                                startActivity(intent);
                            } else {
                                //If email already registered.
                                notifyUser(messageErrorFromCode(task.getException(), SignUpActivity.this));

                            }
                        }
                    });

            binding.buttonGoToLogin.setOnClickListener((View w) -> {
                startActivity(new Intent(this, SignInActivity.class));
                finish();
            });

        });
    }

    public void notifyUser(String str){
        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        Snackbar.make(binding.coordinatorLayoutSignUp, str, Snackbar.LENGTH_SHORT).show();
    }

}
