package com.example.progettomobile.smartwallet.Model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.text.Layout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;

import com.example.progettomobile.smartwallet.R;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import static android.support.v4.content.ContextCompat.startActivity;


public class Utility {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String messageErrorFromCode(Exception code, Context context){
        String message = "";
        if (code instanceof FirebaseAuthUserCollisionException)
        {
            message = context.getString(R.string.email_already_used);

        }else if (code instanceof FirebaseAuthInvalidCredentialsException) {
            message = context.getString(R.string.wrong_password);

        }else if (code instanceof FirebaseAuthWeakPasswordException) {

            message = context.getString(R.string.weak_password);

        } else if(code instanceof FirebaseAuthInvalidUserException) {
            String errorCode =
                    ((FirebaseAuthInvalidUserException) code).getErrorCode();

            if (errorCode.equals("ERROR_USER_NOT_FOUND")) {
                message = Resources.getSystem().getString(R.string.no_account_with_email);

            } else if (errorCode.equals("ERROR_USER_DISABLED")) {
                message = context.getString(R.string.account_disabled);

            } else if (errorCode.equals("ERROR_CREDENTIAL_ALREADY_IN_USE")) {
                message = context.getString(R.string.account_in_use_elsewhere);

            } else if (errorCode.equals("ERROR_INVALID_EMAIL")) {
                message = context.getString(R.string.email_password_wrong_format);

            } else if (errorCode.equals("ERROR_WRONG_PASSWORD")) {
                message = context.getString(R.string.wrong_password);

            }
        }

        return message;
    }

    public static String getIDForTransaction() {
        return UUID.randomUUID().toString();
    }

    public static String getStringDate(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(date);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = monthFromNumber(cal.get(Calendar.MONTH));
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        return  day + " " + month + " " + year;
    }

    public static String getMonthYear(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(date);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = monthFromNumber(cal.get(Calendar.MONTH) + 1);
        return  month + "-" + year;
    }

    public static int[] getYearMonthDay(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return new int[] {year, month, day};
    }

    public static String getMonthYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = monthFromNumber(cal.get(Calendar.MONTH) + 1);
        return  month + "-" + year;
    }

    public static String monthFromNumber(int i) {
        String month = "";
        switch (i) {
            case 1: month = "Gennaio"; break;
            case 2: month = "Febbraio"; break;
            case 3: month = "Marzo"; break;
            case 4: month = "Aprile"; break;
            case 5: month = "Maggio"; break;
            case 6: month = "Giugno"; break;
            case 7: month = "Luglio"; break;
            case 8: month = "Agosto"; break;
            case 9: month = "Settembre"; break;
            case 10: month = "Ottobre"; break;
            case 11: month = "Novembre"; break;
            case 12: month = "Dicembre"; break;
        }
        return month;
    }

    public static String getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth()+1;
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return getStringDate(calendar.getTime());
    }

    public static Date getDateFromDatePickerD(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
