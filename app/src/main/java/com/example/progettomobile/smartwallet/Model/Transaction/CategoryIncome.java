package com.example.progettomobile.smartwallet.Model.Transaction;

import com.example.progettomobile.smartwallet.R;

public enum CategoryIncome {

    GIFT("GIFT", R.drawable.ic_gift),
    OTHER("OTHER", R.drawable.ic_other),
    SALARY("SALARY", R.drawable.ic_salary),
    RENT("RENT", R.drawable.ic_rent),
    REFUND("REFUND", R.drawable.ic_refund);

    private final String name;
    private final int path;

    CategoryIncome(final String n, final int path) {
        this.name = n;
        this.path = path;
    }

    public boolean equals(String otherName, int otherPath) {
        return name.equals(otherName) && (path == otherPath);
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public boolean equalsPath(int otherPath) {
        // (otherPath == null) check is not needed because name.equals(null) returns false
        return path == otherPath;
    }

    public String getName() { return this.name; }

    public int getPath() { return this.path; }

    public String toString() {
        return this.name;
    }
}
