package com.example.progettomobile.smartwallet.Controller;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class StatisticsActivity extends BaseActivity implements View.OnClickListener{

    private List<SliceValue> pieData;
    private PieChartView pieChartView;
    private PieChartData pieChartData;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private double expenses = 0.0;
    private double income;
    private double total;
    private String amount = "0.00";
    private Calendar date = Calendar.getInstance();
    private TextView textViewExpensesValue;
    private TextView textViewIncomeValue;
    private TextView textViewMonth;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        view = findViewById(R.id.coordinatorLayoutStatistics);
        if(Utility.isNetworkAvailable(this)) {
            pieChartView = findViewById(R.id.pieChart);
            textViewExpensesValue = findViewById(R.id.textViewExpensesValue);
            textViewIncomeValue = findViewById(R.id.textViewIncomeValue);
            textViewMonth = findViewById(R.id.textViewMonth);
            textViewMonth.setText(Utility.getMonthYear());
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();

            String monthYear = getIntent().getStringExtra("monthYear");
            if (monthYear != null && !monthYear.isEmpty()) {
                textViewMonth.setText(monthYear);
                getDataFromRealTimeDatabase();
            } else {
                getDataFromRealTimeDatabase();
            }

            textViewMonth.setOnClickListener(v -> {
                new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, (view, y, m, d) -> {
                    date.set(y, m, d);

                    textViewMonth.setText(Utility.monthFromNumber(m + 1) + "-" + y);
                    getDataFromRealTimeDatabase();
                }, date.get(Calendar.YEAR),
                        date.get(Calendar.MONTH),
                        date.get(Calendar.DAY_OF_MONTH)).show();

            });
        } else {
            createSnackbarConnection(view);
        }
    }

    private void createPieChart(String amount, double income, double expenses) {
        pieData = new ArrayList<>();
        pieData.add(new SliceValue((float)income, getResources().getColor(R.color.colorGreenDark)).setLabel(getResources().getString(R.string.text_button_income)));
        pieData.add(new SliceValue((float)expenses, getResources().getColor(R.color.colorRed)).setLabel(getResources().getString(R.string.text_button_expenses)));
        pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1(getResources().getString(R.string.monthly_balance)).setCenterText1FontSize(20).setCenterText1Color(getResources().getColor(R.color.lightBlue));
        pieChartData.setHasCenterCircle(true).setCenterText2(amount+" €").setCenterText2FontSize(20).setCenterText2Color(getResources().getColor(R.color.lightBlue));
        pieChartView.setPieChartData(pieChartData);
        textViewExpensesValue.setText(String.format("%.2f", expenses)+" €");
        textViewIncomeValue.setText(String.format("%.2f", income)+" €");
    }

    private void getDataFromRealTimeDatabase() {

        if(Utility.isNetworkAvailable(this)) {

            Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid()).child(textViewMonth.getText().toString());
            recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    boolean created = false;
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                        if (postSnapshot.getKey() != null) {
                            if (postSnapshot.getKey().equals("amount")) {

                            } else {
                                String transaction = postSnapshot.getKey();
                                Query getTransaction = mDatabase.child("transactions").child(transaction);
                                created = true;
                                expenses = 0.00;
                                income = 0.00;
                                getTransaction.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            if (postSnapshot.getKey().equals("amount")) {
                                                total = Double.valueOf(postSnapshot.getValue().toString());
                                            } else if (postSnapshot.getKey().equals("type")) {
                                                if (postSnapshot.getValue().toString().equals("EXPENSES")) {
                                                    expenses += total;
                                                }
                                                if (postSnapshot.getValue().toString().equals("INCOME")) {
                                                    income += total;
                                                }
                                            }
                                        }
                                        amount = String.format("%.2f", income - expenses);
                                        createPieChart(amount, income, expenses);
                                    }


                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }


                        }
                    if (!created) {
                        createPieChart("0.00", 0.00, 0.00);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w("STATISTICS_ACT", "loadPost:onCancelled", databaseError.toException());
                }
            });
        } else {
            createSnackbarConnection(view);
        }

    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

}
