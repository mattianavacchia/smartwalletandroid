package com.example.progettomobile.smartwallet.Controller;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivitySettingsBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class SettingsActivity extends BaseActivity implements View.OnClickListener{

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ActivitySettingsBinding binding;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        view = findViewById(R.id.coordinatorLayoutSettings);
        if(Utility.isNetworkAvailable(this)) {
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            binding.buttonDelete.setOnClickListener((View v) -> {
                Utility.hideKeyboard(this);
                Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid());
                ((DatabaseReference) recentPostsQuery).removeValue();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, SignInActivity.class));
            });

            binding.buttonSave.setOnClickListener((View v) -> {
                Utility.hideKeyboard(this);
                Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid());
                FirebaseUser user = mAuth.getCurrentUser();
                String newEmail = binding.editTextEmail.getText().toString();
                String newPassword = binding.editTextPassword.getText().toString();
                if ((!newEmail.equals("")) && (!newEmail.equals(((DatabaseReference) recentPostsQuery).child("email").getKey()))) {
                    ((DatabaseReference) recentPostsQuery).child("email").setValue(newEmail);
                    notifyUser(getString(R.string.changed_successfully));
                }
                if ((!newPassword.equals(""))) {
                    user.updatePassword(newPassword);
                    notifyUser(getString(R.string.changed_successfully));
                }
            });
        } else {
            createSnackbarConnection(view);
        }
    }

    public void notifyUser(String str){
        Snackbar.make(binding.coordinatorLayoutSettings, str, Snackbar.LENGTH_SHORT).show();
    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

}
