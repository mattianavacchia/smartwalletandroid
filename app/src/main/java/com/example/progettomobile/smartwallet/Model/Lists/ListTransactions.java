package com.example.progettomobile.smartwallet.Model.Lists;

import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListTransactions implements Serializable {

    private String id;
    private String name;
    private List<Transaction> transactionList = new ArrayList<>();
    private String color = "";

    public ListTransactions() {
        id = "";
        name = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
