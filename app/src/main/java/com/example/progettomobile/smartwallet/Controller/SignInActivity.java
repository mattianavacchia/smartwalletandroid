package com.example.progettomobile.smartwallet.Controller;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivitySigninBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static com.example.progettomobile.smartwallet.Model.Utility.messageErrorFromCode;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "login";
    private FirebaseAuth mAuth;
    private ActivitySigninBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signin);
        mAuth = FirebaseAuth.getInstance();
        binding.buttonLogin.setOnClickListener((View v)->{
            if (Utility.isNetworkAvailable(getApplicationContext())) {
                mAuth.signInWithEmailAndPassword(binding.editTextEmailSignIn.getText().toString(), binding.editTextPasswordSignIn.getText().toString())
                        .addOnCompleteListener(this, task -> {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = mAuth.getCurrentUser();
                                notifyUser(getString(R.string.login_successful));
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            } else {
                                // If sign in fails, display a message to the user.
                                notifyUser(messageErrorFromCode(task.getException(), this));
                            }

                });
            } else
                notifyUser(getString(R.string.error_no_connection));
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    public void goToRegistration(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    public void notifyUser(String str){
        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        Snackbar.make(binding.coordinatorLayoutSignIn, str, Snackbar.LENGTH_SHORT).show();
    }
}
