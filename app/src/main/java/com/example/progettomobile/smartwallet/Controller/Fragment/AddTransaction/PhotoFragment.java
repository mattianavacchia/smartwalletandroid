package com.example.progettomobile.smartwallet.Controller.Fragment.AddTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.progettomobile.smartwallet.Model.Transaction.TransactionManager;
import com.example.progettomobile.smartwallet.R;

import java.util.Objects;

public class PhotoFragment extends Fragment {

    public static final int TAKEFROMCAMERACODE = 0;
    public static final int TAKEFROMGALLERYCODE = 1;

    View mRootView;
    Uri uriImage;
    ImageView mImageViewSelectedFragmentPhoto;
    LinearLayout mLayoutButtonFragmentPhoto;
    Button mButtonTakeFromLibraryPhotoFragment, mButtonTakeFromCameraPhotoFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_photo, container, false);

        mImageViewSelectedFragmentPhoto = mRootView.findViewById(R.id.imageViewSelectedFragmentPhoto);
        mLayoutButtonFragmentPhoto = mRootView.findViewById(R.id.layoutButtonFragmentPhoto);
        mButtonTakeFromCameraPhotoFragment = mRootView.findViewById(R.id.buttonTakeFromCameraPhotoFragment);
        mButtonTakeFromCameraPhotoFragment.setOnClickListener(v -> {
            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePicture, TAKEFROMCAMERACODE);
        });
        mButtonTakeFromLibraryPhotoFragment = mRootView.findViewById(R.id.buttonTakeFromLibraryPhotoFragment);
        mButtonTakeFromLibraryPhotoFragment.setOnClickListener(v -> {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto , TAKEFROMGALLERYCODE);
        });

        // update image saved
        /*if (!TransactionManager.getPathPhoto().equals("")) {
            mImageViewSelectedFragmentPhoto.setImageURI(Uri.parse(TransactionManager.getPathPhoto()));
        }*/

        return mRootView;
    }

    public void setImage() {
        mImageViewSelectedFragmentPhoto.setImageURI(Uri.parse(TransactionManager.getPathPhoto()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            uriImage = data.getData();
            TransactionManager.setPathPhoto(String.valueOf(uriImage));
            mImageViewSelectedFragmentPhoto.setImageURI(uriImage);
        }
    }
}
