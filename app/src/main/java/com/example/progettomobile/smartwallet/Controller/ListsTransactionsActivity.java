package com.example.progettomobile.smartwallet.Controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.progettomobile.smartwallet.Controller.Adapter.ListsAdapter;
import com.example.progettomobile.smartwallet.Controller.Fragment.AddNewListDialogFragment;
import com.example.progettomobile.smartwallet.Controller.Listener.RecyclerTouchListener;
import com.example.progettomobile.smartwallet.Model.Lists.ListTransactions;
import com.example.progettomobile.smartwallet.Model.Lists.ListsManager;
import com.example.progettomobile.smartwallet.Model.Utility;
import com.example.progettomobile.smartwallet.R;
import com.example.progettomobile.smartwallet.databinding.ActivityListsTransactionsBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListsTransactionsActivity extends BaseActivity implements AddNewListDialogFragment.AddNewListDialogListener, View.OnClickListener{

    private ActivityListsTransactionsBinding binding;
    private int menuToChoose = R.menu.drawer_menu_lists;
    private ListsAdapter listsAdapter;
    private List<ListTransactions> listTransactions = new ArrayList<>();
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private int positionSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists_transactions);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_lists_transactions);
        if(Utility.isNetworkAvailable(this)) {
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();

            listsAdapter = new ListsAdapter(this.listTransactions, getApplicationContext());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            binding.recyclerViewLists.setLayoutManager(mLayoutManager);
            binding.recyclerViewLists.setItemAnimator(new DefaultItemAnimator());
            binding.recyclerViewLists.setAdapter(listsAdapter);

            binding.recyclerViewLists.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), binding.recyclerViewLists, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Intent intent = new Intent(getApplicationContext(), SingleListTransactionsActivity.class);
                    intent.putExtra("list", listTransactions.get(position));
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {
                    positionSelected = position;
                    menuToChoose = R.menu.drawer_menu_lists_selected;
                    invalidateOptionsMenu();

                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    view.findViewById(R.id.linearLayoutListOnFirebaseListRow).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#95a5a6")));
                }
            }));

            getIDListFromUser();
        } else {
            createSnackbarConnection(binding.recyclerViewLists);
        }
    }

    @Override
    public void onBackPressed() {
        if (menuToChoose == R.menu.drawer_menu_lists_selected) {
            menuToChoose = R.menu.drawer_menu_lists;
            invalidateOptionsMenu();
            binding.recyclerViewLists.getChildAt(positionSelected)
                    .setBackgroundTintList(
                            ColorStateList.valueOf(
                                    Color.parseColor(listsAdapter.getColorAt(positionSelected))));
        } else {
            super.onBackPressed();
        }
    }

    private void getIDListFromUser() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Operazione in corso...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Query recentPostsQuery = mDatabase.child("users").child(mAuth.getUid()).child("lists");
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    getList(postSnapshot.getKey());
                }

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());

                progressDialog.dismiss();
            }
        });
    }

    private void getList(String id) {
        Query recentPostsQuery = mDatabase.child("lists").child(id);
        recentPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ListTransactions list = new ListTransactions();
                list.setId(id);
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    if (postSnapshot.getKey().equals("name")) {
                        list.setName(postSnapshot.getValue().toString());
                    }
                }
                listTransactions.add(list);
                listsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("MAINACTIVITY_SW", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuToChoose = R.menu.drawer_menu_lists;
        invalidateOptionsMenu();

        switch (item.getItemId()) {
            case R.id.add:
                openDialog();
                return true;
            case R.id.remove:
                mDatabase.child("lists").child(listTransactions.get(positionSelected).getId()).removeValue();
                mDatabase.child("users").child(mAuth.getUid()).child("lists").child(listTransactions.get(positionSelected).getId()).removeValue();
                listTransactions.remove(positionSelected);
                listsAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(menuToChoose, menu);
        return true;
    }

    public void openDialog() {
        AddNewListDialogFragment exampleDialog = new AddNewListDialogFragment();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");
    }

    @Override
    public void applyTexts(String name) {
        String id = ListsManager.getNewID();
        mDatabase.child("lists").child(id).child("name").setValue(name)
                .addOnSuccessListener(aVoid -> {
                    Snackbar.make(binding.coordinatorLayoutListsTransactions, "Lista aggiunta con successo.", Snackbar.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> Snackbar.make(binding.coordinatorLayoutListsTransactions, "Errore nell'aggiungere la lista. Riprovare.", Snackbar.LENGTH_SHORT).show());
        mDatabase.child("lists").child(id).child("people").child(mAuth.getUid()).setValue(true);
        mDatabase.child("users").child(mAuth.getUid()).child("lists").child(id).setValue(true);

        ListTransactions list = new ListTransactions();
        list.setId(id);
        list.setName(name);
        listTransactions.add(list);
        listsAdapter.notifyDataSetChanged();
    }

    public void createSnackbarConnection(View view){
        Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_SHORT);
        snackbar.setAction(getResources().getString(R.string.settings), this);
        snackbar.setActionTextColor(getResources().getColor(R.color.colorRed));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

}
