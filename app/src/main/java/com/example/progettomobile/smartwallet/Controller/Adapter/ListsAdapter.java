package com.example.progettomobile.smartwallet.Controller.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.progettomobile.smartwallet.Model.Lists.ListTransactions;
import com.example.progettomobile.smartwallet.Model.Transaction.Transaction;
import com.example.progettomobile.smartwallet.R;

import java.util.List;
import java.util.Random;

public class ListsAdapter extends RecyclerView.Adapter<ListsAdapter.MyViewHolder> {

    private String[] colors = new String[] { "#1DE9B6", "#FF6D00", "#DD2C00", "#00C853", "#0091EA", "#C51162", "#D50000" };
    private List<ListTransactions> listTransactionsList;
    private Context context;
    Random rand = new Random();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public TextView name;

        public MyViewHolder(View view) {
            super(view);
            layout = view.findViewById(R.id.linearLayoutListOnFirebaseListRow);
            name = view.findViewById(R.id.textViewNameListRow);
        }
    }

    public ListsAdapter(List<ListTransactions> transactionsList, Context context) {
        this.context = context;
        this.listTransactionsList = transactionsList;
    }

    @NonNull
    @Override
    public ListsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lists_on_firebase_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListsAdapter.MyViewHolder myViewHolder, int i) {
        ListTransactions transactions = listTransactionsList.get(i);
        myViewHolder.name.setText(transactions.getName());
        listTransactionsList.get(i).setColor(this.colors[rand.nextInt(colors.length)]);
        myViewHolder.layout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(listTransactionsList.get(i).getColor())));
    }

    public String getColorAt(int position) {
        return this.listTransactionsList.get(position).getColor();
    }

    @Override
    public int getItemCount() {
        return listTransactionsList.size();
    }
}
