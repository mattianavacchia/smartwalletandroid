package com.example.progettomobile.smartwallet.Controller.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.progettomobile.smartwallet.R;

public class AddNewListDialogFragment extends AppCompatDialogFragment {
    private EditText editTextName;
    private AddNewListDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_add_new_list, null);

        builder.setView(view)
                .setTitle("Lista Amici")
                .setNegativeButton("ANNULLA", (dialogInterface, i) -> {

                })
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    String name = editTextName.getText().toString();
                    listener.applyTexts(name);
                });

        editTextName = view.findViewById(R.id.edit_username);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (AddNewListDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ExampleDialogListener");
        }
    }

    public interface AddNewListDialogListener {
        void applyTexts(String name);
    }
}
